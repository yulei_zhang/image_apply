import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import torch
import torch.optim as optim
from torch.utils.data import Dataset
import numpy as np


# from torchsummary import summary

def conv3x3(in_planes, out_planes, stride=1, groups=1):
    """3x3 convolution with padding"""
    return nn.Conv2d(
        in_planes, out_planes, kernel_size=3, stride=stride,
        padding=1, groups=groups, bias=False
    )


def conv1x1(in_planes, out_planes, stride=1):
    """1x1 convolution"""
    return nn.Conv2d(in_planes, out_planes, kernel_size=1, stride=stride, bias=False)


class BasicBlock(nn.Module):
    expansion = 1

    def __init__(
            self, inplanes, planes, stride=1, downsample=None, groups=1,
            base_width=64, norm_layer=None
    ):
        super(BasicBlock, self).__init__()
        if norm_layer is None:
            norm_layer = nn.BatchNorm2d
        if groups != 1 or base_width != 64:
            raise ValueError('BasicBlock only supports groups=1 and base_width=64')
        # Both self.conv1 and self.downsample layers downsample the input when stride != 1
        self.conv1 = conv3x3(inplanes, planes, stride)
        self.bn1 = norm_layer(planes)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv3x3(planes, planes)
        self.bn2 = norm_layer(planes)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        identity = x
        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)
        out = self.conv2(out)
        out = self.bn2(out)
        if self.downsample is not None:
            identity = self.downsample(x)
        out += identity
        out = self.relu(out)
        return out


class Bottleneck(nn.Module):
    expansion = 4

    def __init__(
            self, inplanes, planes, stride=1, downsample=None, groups=1,
            base_width=64, norm_layer=None
    ):
        super(Bottleneck, self).__init__()
        if norm_layer is None:
            norm_layer = nn.BatchNorm2d
        width = int(planes * (base_width / 64.)) * groups
        # Both self.conv2 and self.downsample layers downsample the input when stride != 1
        self.conv1 = conv1x1(inplanes, width)
        self.bn1 = norm_layer(width)
        self.conv2 = conv3x3(width, width, stride, groups)
        self.bn2 = norm_layer(width)
        self.conv3 = conv1x1(width, planes * self.expansion)
        self.bn3 = norm_layer(planes * self.expansion)
        self.relu = nn.ReLU(inplace=True)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        identity = x
        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)
        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)
        out = self.conv3(out)
        out = self.bn3(out)
        if self.downsample is not None:
            identity = self.downsample(x)
        out += identity
        out = self.relu(out)
        return out


class ResNet(nn.Module):
    BN_MOMENTUM = 0.1

    def __init__(
            self, block, layers, nb_ch, num_classes, zero_init_residual=False,
            groups=1, width_per_group=64, norm_layer=None
    ):
        super(ResNet, self).__init__()
        if norm_layer is None:
            norm_layer = nn.BatchNorm2d

        self.inplanes = 64
        self.groups = groups
        self.deconv_with_bias = False
        self.base_width = width_per_group
        self.conv1 = nn.Conv2d(nb_ch, self.inplanes, kernel_size=7, stride=2, padding=3, bias=False)
        self.bn1 = norm_layer(self.inplanes)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, 64, layers[0], norm_layer=norm_layer)
        self.layer2 = self._make_layer(block, 128, layers[1], stride=2, norm_layer=norm_layer)
        self.layer3 = self._make_layer(block, 256, layers[2], stride=2, norm_layer=norm_layer)
        self.layer4 = self._make_layer(block, 512, layers[3], stride=2, norm_layer=norm_layer)
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        self.fc = nn.Sequential(
            nn.Linear(512 * block.expansion, 2),
            nn.ReLU(inplace=True)
        )
        self.prop = nn.Sequential(
            nn.Linear(64 + 78, 128),
            nn.ReLU(inplace=True)
        )
        self.prop2 = nn.Sequential(
            nn.Linear(128, 128),
            nn.ReLU(inplace=True)
        )

        self.classify = nn.Linear(512 * block.expansion, num_classes)

        # deconv-layers
        self.deconv_layers = self._make_deconv_layer(
            deconv_design["NUM_DECONV_LAYERS"],
            deconv_design["NUM_DECONV_FILTERS"],
            deconv_design["NUM_DECONV_KERNELS"],
        )
        self.final_layer = nn.Conv2d(
            in_channels=deconv_design["NUM_DECONV_FILTERS"][-1],
            out_channels=1,
            kernel_size=1,
            stride=1,
            padding=0
        )

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, (nn.BatchNorm2d, nn.GroupNorm)):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

        if zero_init_residual:
            for m in self.modules():
                if isinstance(m, Bottleneck):
                    nn.init.constant_(m.bn3.weight, 0)
                elif isinstance(m, BasicBlock):
                    nn.init.constant_(m.bn2.weight, 0)

    def _make_layer(self, block, planes, blocks, stride=1, norm_layer=None):
        if norm_layer is None:
            norm_layer = nn.BatchNorm2d
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                conv1x1(self.inplanes, planes * block.expansion, stride),
                norm_layer(planes * block.expansion, momentum=ResNet.BN_MOMENTUM),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample, self.groups,
                            self.base_width, norm_layer))
        self.inplanes = planes * block.expansion
        for _ in range(1, blocks):
            layers.append(block(self.inplanes, planes, groups=self.groups,
                                base_width=self.base_width, norm_layer=norm_layer))

        return nn.Sequential(*layers)

    def _get_deconv_cfg(self, deconv_kernel, index):
        if deconv_kernel == 4:
            padding = 1
            output_padding = 0
        elif deconv_kernel == 3:
            padding = 1
            output_padding = 1
        elif deconv_kernel == 2:
            padding = 0
            output_padding = 0
        return deconv_kernel, padding, output_padding

    def _make_deconv_layer(self, num_layers, num_filters, num_kernels):
        ## num_layers=3, num_filters=[256,256,256], num_kernels=[4,4,4]
        assert num_layers == len(num_filters), '__invalid_size__: num_deconv_layers != len(num_deconv_filters)'
        assert num_layers == len(num_kernels), '__invalid_size__: num_deconv_layers != len(num_deconv_filters)'

        layers = []
        for i in range(num_layers):
            kernel, padding, output_padding = \
                self._get_deconv_cfg(num_kernels[i], i)

            planes = num_filters[i]
            layers.append(
                nn.ConvTranspose2d(
                    in_channels=self.inplanes,
                    out_channels=planes,
                    kernel_size=kernel,
                    stride=2,
                    padding=padding,
                    output_padding=output_padding,
                    bias=self.deconv_with_bias))
            layers.append(nn.BatchNorm2d(planes, momentum=ResNet.BN_MOMENTUM))
            layers.append(nn.ReLU(inplace=True))
            self.inplanes = planes

        return nn.Sequential(*layers)

    def forward(self, x, debug=False):

        if debug: print('[step-01]: {}'.format(x.shape))
        x = self.conv1(x)
        if debug: print('[step-02]: {}'.format(x.shape))
        x = self.bn1(x)
        if debug: print('[step-03]: {}'.format(x.shape))
        x = self.relu(x)
        if debug: print('[step-04]: {}'.format(x.shape))
        x = self.maxpool(x)
        if debug: print('[step-05]: {}'.format(x.shape))

        x = self.layer1(x)
        if debug: print('[step-L1]: {}'.format(x.shape))
        x = self.layer2(x)
        if debug: print('[step-L2]: {}'.format(x.shape))
        x = self.layer3(x)
        if debug: print('[step-L3]: {}'.format(x.shape))
        x = self.layer4(x)
        if debug: print('[step-L4]: {}'.format(x.shape))

        # deconv:
        # y = x
        # y = self.deconv_layers(y)
        # if debug: print('[step-H1]: {}'.format(y.shape))
        # y = self.final_layer(y)
        # if debug: print('[step-H2]: {}'.format(y.shape))

        x = self.avgpool(x)
        if debug: print('[step-06]: {}'.format(x.shape))
        x = x.view(x.size(0), -1)
        if debug: print('[step-07]: {}'.format(x.shape))
        x = self.classify(x)
        if debug: print('[step-08]: {}'.format(x.shape))
        # x = nn.Sigmoid()(x)
        # if debug: print('[step-09]: {}'.format(x.shape))
        return x  # , y


def resnet152(nb_ch, num_classes):
    model = ResNet(Bottleneck, [3, 8, 36, 3], nb_ch, num_classes)
    return model


def resnet101(nb_ch, num_classes):
    model = ResNet(Bottleneck, [3, 4, 23, 3], nb_ch, num_classes)
    return model


def resnet50(nb_ch, num_classes):
    model = ResNet(Bottleneck, [3, 4, 6, 3], nb_ch, num_classes)
    return model


def resnet18(nb_ch, num_classes):
    model = ResNet(Bottleneck, [2, 2, 2, 2], nb_ch, num_classes)
    return model


resnet_spec = {
    18: (BasicBlock, [2, 2, 2, 2]),
    34: (BasicBlock, [3, 4, 6, 3]),
    50: (Bottleneck, [3, 4, 6, 3]),
    101: (Bottleneck, [3, 4, 23, 3]),
    152: (Bottleneck, [3, 8, 36, 3])
}
deconv_design = {
    "NUM_DECONV_LAYERS": 4,
    "NUM_DECONV_FILTERS": [256, 256, 256, 256],
    "NUM_DECONV_KERNELS": [4, 4, 4, 4],
}
