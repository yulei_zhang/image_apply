import argparse
import json
import os

import numpy as np
import pandas as pd
import xgboost as xgb
from sklearn import metrics
from sklearn.model_selection import train_test_split

from llp_analysis.plotting import plotting_cm, plot_hist
from llp_analysis.train import opt


def process(mass, lifetime, input_dir: str, num_iteration: int = 10, cv_number=None, cv_current=None):
    n_Higgs = 1e6

    # CNN
    signal_total = [
        0.201954 / (1 + 0.201954) * 0.692 * n_Higgs,
        0.201954 / (1 + 0.201954) * 0.205 * n_Higgs,
        1 / (1 + 0.201954) * 0.692 * n_Higgs,
        1 / (1 + 0.201954) * 0.205 * n_Higgs,
    ]

    bkg_total = [
        5e6,
        5e6,
        1e8,
        n_Higgs,
    ]

    in_features = ['2f', '4f', 'nDLLP_0', 'nDLLP_1', 'nDLLP_2']

    weight_balance = sum(bkg_total) / sum(signal_total)

    # df_single = {
    #     s: read_npz(os.path.join(input_dir, f'{s}.npz'), sample_total=w) for s, w in
    #     zip(sample_list, weight_list)
    # }
    #
    # df_total = pd.concat(df_single.values())

    # df_bkg = pd.concat(df for key, df in df_single.items() if key in bkg_list)
    # df_sig = {key: df for key, df in df_single.items() if key in signal_list}

    signal_list = [2, 3, 4]
    sample_weight = {
        1: 1e8,  # eeqq
        2: 1e6,  # ZH
        3: 5e6,  # uudd
        4: 5e6,  # cscs
        5: 0.201954 / (1 + 0.201954) * 0.692 * n_Higgs,  # 2j_Zbjbj
        6: 0.201954 / (1 + 0.201954) * 0.205 * n_Higgs,  # 2j_Zvv
        7: 1 / (1 + 0.201954) * 0.692 * n_Higgs,  # 4j_Zbjbj
        8: 1 / (1 + 0.201954) * 0.205 * n_Higgs,  # 4j_Zvv
    }

    df_total = pd.DataFrame(np.load(input_dir), columns=(['truth_label'] + in_features + ['sample_label']))
    df_total['cv'] = df_total.index % cv_number if cv_number is not None else 0
    df_total['weight'] = df_total['sample_label'].map(sample_weight)
    # df_total_label_dict = {
    #     k + 1: v for k, v in
    #     enumerate(df_total.groupby('sample_label')['weight'].count().to_numpy())
    # }
    total_count = df_total.groupby('sample_label')['weight'].count()
    df_total_label_dict = {
        k: v for k, v in zip(total_count.index.to_numpy(), total_count.to_numpy())
    }
    df_total['sum_weight'] = df_total['sample_label'].map(df_total_label_dict)
    df_total['weight'] = df_total['weight'] / df_total['sum_weight']

    df_total['predicts'] = df_total[in_features].to_numpy().argmax(axis=1)

    df_cm = df_total.copy(deep=True)

    for i in range(5):
        if i not in df_cm['truth_label'].values:
            tmp_df = pd.DataFrame(
                [[i, 0, 0, 0, 0, 0, i, -1, -1, i, 0]],
                columns=df_cm.columns
            )
            df_cm = pd.concat([df_cm, tmp_df])
    cm_total = metrics.confusion_matrix(
        df_cm['truth_label'], df_cm['predicts'],
        sample_weight=df_cm['weight'],
    )

    # print(cm_total)
    if cv_current is not None:
        df_total_train = df_total[df_total['cv'] != cv_current].copy()
        df_total_eval = df_total[df_total['cv'] == cv_current].copy()
    else:
        df_total_train = df_total
        df_total_eval = df_total

    df_bkg = df_total_train.loc[df_total_train['truth_label'].isin([0, 1])]
    df_sig = {key: df_total_train.loc[df_total_train['truth_label'] == key] for key in signal_list}

    X = df_total_train[in_features]
    Y = df_total_train['truth_label'].isin(signal_list)

    x_train, x_test, y_train, y_test = \
        train_test_split(X.values, Y.values, test_size=0.25)

    dtrain = xgb.DMatrix(
        data=x_train, label=y_train, weight=None,
        feature_names=in_features
    )
    dtest = xgb.DMatrix(
        data=x_test, label=y_test, weight=None,
        feature_names=in_features
    )

    dbkg = xgb.DMatrix(
        data=df_bkg[in_features].values, label=np.zeros(len(df_bkg)), weight=df_bkg['weight'],
        feature_names=in_features
    )

    dsig = {
        key: xgb.DMatrix(
            data=df[in_features].values, label=np.ones(len(df)), weight=df['weight'],
            feature_names=in_features
        ) for key, df in df_sig.items()
    }

    dataset = {
        "dtrain": dtrain,
        "dtest": dtest,
        "dbkg": dbkg,
        "dsig": dsig,
    }

    result = opt(dataset, weight_balance, num_iteration)

    # eval best model to full dataset
    X = df_total_eval[in_features]
    Y = df_total_eval['truth_label'].isin(signal_list)

    full_data = xgb.DMatrix(data=X.values, label=Y.values, feature_names=in_features)
    full_data_result = result.best_trial['result']['best_model'].predict(
        full_data,
        iteration_range=(0, result.best_trial['result']['best_model'].best_iteration + 1)
    )

    df_total_eval.loc[:, 'xgboost_score'] = full_data_result
    # plot_hist(df_total, result.best_trial['result']['bkg_cut'])

    bkg_cut = max(df_total_eval.loc[~df_total_eval['truth_label'].isin(signal_list), 'xgboost_score'])
    df_total_eval.loc[:, 'final_score'] = full_data_result > bkg_cut
    df_sig = df_total_eval.loc[
        df_total_eval['final_score'], ['weight', 'sample_label', 'truth_label', 'predicts']
    ].reset_index(drop=True)

    # core result
    # 2-jet, 4-jet in nDLLP_1 and nDLLP_2 SR region
    # sample_label --> 2-jet: 5,6
    #              --> 4-jet: 7,8
    # predicts     --> nDLLP_0 Region: 2 (check region)
    #              --> nDLLP_1 Region: 3
    #              --> nDLLP_2 Region: 4
    # results: --> X[2,2]
    #           nDLLP_0   nDLLP_1   nDLLP_2
    #   2-jet     X00       X01      X02
    #   4-jet     X10       X11      X12

    X = [[
        df_sig.loc[(df_sig['sample_label'].isin(i)) & (df_sig['predicts'] == j), 'weight'].sum()
        for i in [(5, 6), (7, 8)]
    ] for j in [2, 3, 4]
    ]

    return result, cm_total, X


def main_analysis(args, cv_current=None):
    print(args)

    t, cm_total, X = process(**vars(args), cv_current=cv_current)

    mass = args.mass

    def calc_eff(i, l):
        return sum([v[i] for k, v in t.best_trial['result']['eff'].items() if l == k])

    output = {
        "mass": mass,
        "lifetime": args.lifetime,
        "sig_eff_total": -t.best_trial['result']['loss'],
        "sig_eff_0_nDLLP": calc_eff(0, 2) / calc_eff(1, 2),
        "sig_eff_1_nDLLP": calc_eff(0, 3) / calc_eff(1, 3),
        "sig_eff_2_nDLLP": calc_eff(0, 4) / calc_eff(1, 4),
        "sig_eff": t.best_trial['result']['eff'],
        "final_result": X,
    }

    # Serializing json
    json_object = json.dumps(output, indent=4)

    # Writing to output
    out_json_name = f"out_{mass}_{args.lifetime}ns.json" if cv_current is None else os.path.join(
        'cv',
        f"out_{mass}_{args.lifetime}ns.{cv_current}.json"
    )
    with open(out_json_name, "w") as outfile:
        outfile.write(json_object)

    cm = [value for value in t.best_trial['result']['eff']['cm'].values()]
    cm_label = ['0-DLLP', '1-DLLP', '2-DLLP']
    cm_label_total = ['2-fermion', '4-fermion', '0-DLLP', '1-DLLP', '2-DLLP']

    plotting_cm(cm, cm_label, mass, args.lifetime, norm=True, extra_title=f'cv_{cv_current}')
    plotting_cm(cm, cm_label, mass, args.lifetime, norm=False, extra_title=f'cv_{cv_current}')

    plotting_cm(cm_total, cm_label_total, mass, args.lifetime, norm=True, extra_title=f'Total_cv_{cv_current}')

    return out_json_name


def main_wrapper(arg):
    return main_analysis(*arg)


def merge_json(jsons, mass, lifetime):
    old_list = []
    for j in jsons:
        with open(j, 'r') as f:
            old_result = json.loads(f.read())
            old_list.append(old_result)

    if len(old_list) > 0:
        new_json = {
            'mass': old_list[0]['mass'],
            'lifetime': old_list[0]['lifetime'],
            'sig_eff_total': sum(item['sig_eff_total'] for item in old_list) / len(old_list),
            'final_result': sum([np.array(o['final_result']) for o in old_list]).tolist()
        }

        with open(f"out_{mass}_{lifetime}ns.json", "w") as outfile:
            outfile.write(json.dumps(new_json, indent=4))

    pass


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Analysis Program for LLP")
    parser.add_argument('-m', '--mass', type=str, choices=['50', '10', '1'], help="mass point")  # GNN
    parser.add_argument('-l', '--lifetime', type=str, help="Lifetime [ns]")
    parser.add_argument('-i', '--input_dir', type=str, help="input directory for npz")
    parser.add_argument('-n', '--num_iteration', type=int, default=100, help="iteration rounds for optimization")
    parser.add_argument('-c', '--cv_number', type=int, help="using n-fold cv to evaluate")
    args = parser.parse_args()

    if args.cv_number is None:
        main_analysis(args, cv_current=None)
    else:
        # n-fold CV
        import multiprocessing as mp

        os.makedirs('cv', exist_ok=True)
        cv_list = [(args, i) for i in range(args.cv_number)]
        with mp.Pool(processes=args.cv_number) as pool:

            results = pool.map(main_wrapper, cv_list)

            merge_json(results, args.mass, args.lifetime)
        pass
