import time

import numpy as np
from torch.utils.data import Dataset, DataLoader
import torch
import torchvision.transforms as transforms

from data_io.read_root import ImpRoot


class MyDataset(Dataset):
    def __init__(self, image, transform):
        self.image = torch.from_numpy(image)
        self.transform = transform

    def __getitem__(self, index):
        image = self.image[index]

        if self.transform:
            image = self.transform(image)
        return image

    def __len__(self):
        return len(self.image)


def generate_data(dirs: str, itr: int = 1, batch_size: int = 128):
    infile = ImpRoot(itr=itr, debug=True)

    data_out = infile.read_root(dirs=dirs)

    # transforms
    transform = transforms.Normalize(mean=[0.00899863, 0.03670538], std=[0.09760329, 1.5181196])

    while True:
        try:
            image = next(data_out)

            apply_set = MyDataset(image, transform)
            loaders = DataLoader(
                apply_set, batch_size=batch_size,
                shuffle=False, pin_memory=True, num_workers=0
            )

            yield loaders

        except StopIteration:
            yield None


if __name__ == '__main__':
    p = generate_data(r'E:\PycharmProjects\image_apply\workspace\llp_2j_50_10.root:Image', 3)

    r = next(p)
