import gc
import os
import time

import numpy as np
import uproot as up


class ImpRoot:
    """
    Import the root file using uproot
    """

    def __init__(
            self,
            x: int = 200,
            y: int = 200,
            itr: int = 1,
            debug: bool = False
    ):
        self._cn = self.__class__.__name__
        self.debug = debug
        self.itr = itr
        self.x = x
        self.y = y
        self.channels = ['T', 'E']

    def read_root(
            self, dirs: str,
    ):

        with up.open(dirs + ':Image') as in_file:

            start_time = time.time()
            if self.debug:
                print(f'[Import Root] ==> Start reading {dirs}', flush=True)

            # events for each event
            total = in_file.num_entries
            delta_event = int(total / self.itr)

            for batch, report in in_file.iterate(
                    expressions=self.channels, step_size=delta_event, library="np",
                    report=True):
                length = report.stop - report.start
                print(f'[Import Root] ==> Loading {length} [{report.start}, {report.stop}] / {total}', flush=True)
                image = np.concatenate(
                    ([ch.reshape(length, 1, self.x, self.y) for ch in batch.values()]),
                    axis=1)

                yield image

                del image
                gc.collect()


if __name__ == '__main__':
    r = ImpRoot(debug=True)
    p = r.read_root(r'E:\PycharmProjects\image_apply\workspace\llp_2j_50_10.root:Image')

    re = next(p)
