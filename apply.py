import fileinput
import gc
import os
import pathlib

import argparse

import torch
import torch.nn as nn
import resnet

import numpy as np
import torch

from data_io.construct_dataset import generate_data


def fast_apply_process(ds, device, models: list, out_arr):
    for i, inputs in enumerate(ds):
        inputs = inputs.to(device).float()

        for m_idx in range(len(models)):
            outputs = models[m_idx](inputs)
            outputs = nn.Softmax(dim=1)(outputs)
            out_arr[f'model_{m_idx}'] = np.concatenate((out_arr[f'model_{m_idx}'], outputs.cpu().detach().numpy()))

        del inputs, outputs
        torch.cuda.empty_cache()
        gc.collect()


def process_data(dir: str, models: list, device, itr: int):
    torch.backends.cudnn.enabled = True
    torch.backends.cudnn.benchmark = True
    torch.set_grad_enabled(True)  # Context-manager
    # setup random seed for reproduction
    torch.backends.cudnn.deterministic = True
    torch.manual_seed(1234)
    torch.cuda.manual_seed(1234)

    for model in models:
        model.eval()

    out_arr = {f'model_{i}': np.array([]).reshape((-1, 3)) for i in range(len(models))}

    in_data = generate_data(dirs=dir, itr=itr)
    while True:
        ds = next(in_data)
        if ds is None:
            break

        fast_apply_process(ds, device, models, out_arr)

    # in_name = os.path.splitext(dir)[0]
    in_name = pathlib.Path(dir).stem
    print(f'==> Save to {in_name}', flush=True)
    np.savez(in_name, **out_arr)

    print('Apply Done.')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Imaging Apply program for LLPs by Yulei")
    parser.add_argument('--infile', type=str, help="input root file")
    parser.add_argument('--models', type=str, nargs='*', help="Input model lists")
    parser.add_argument('--itr', type=int, default=1, help="iteration number")
    args = parser.parse_args()

    print(args)

    torch.nn.Module.dump_patches = True

    de = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    models = [torch.load(m) for m in args.models]

    process_data(dir=args.infile, models=models, device=de, itr=args.itr)


