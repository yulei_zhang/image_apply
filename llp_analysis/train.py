import numpy as np
import pandas as pd
import xgboost as xgb
from hyperopt import hp, STATUS_OK, Trials, fmin, tpe
from sklearn import metrics
import platform


def train_xgboost(
        dtrain: xgb.DMatrix = None, dtest: xgb.DMatrix = None, dbkg: xgb.DMatrix = None, dsig: dict[xgb.DMatrix] = None,
        train_param=None, debug=False,
):
    # Learning task parameters
    task_param = {
        'num_boost_round': 300,
        'early_stopping_rounds': 10,
        'evals': [(dtrain, "train"), (dtest, "eval")],
        'verbose_eval': 50 if debug else False,
    }

    bst = xgb.train(train_param, dtrain, **task_param)

    # evaluation on test
    preds = bst.predict(dbkg, iteration_range=(0, bst.best_iteration + 1))

    bkg_th = max(preds)
    if debug: print(f'==> 0-bkg threshold: {bkg_th}')

    eff_dict = {}
    passed_w, total_w = 0, 0
    df_sig_passed_pred = pd.DataFrame([])
    for key, df in dsig.items():
        def excep(df_sig, df):
            tmp_df = pd.DataFrame(
                [[0, 0, 0, 0, 0, key, key, -1]],
                columns=df.feature_names + ['truth_label', 'predicts', 'weight']
            )
            df_sig = pd.concat([df_sig, tmp_df])
            eff_dict.update({key: [0, -1]})
            return df_sig

        if len(df.get_data().toarray()) == 0:
            df_sig_passed_pred = excep(df_sig_passed_pred, df)
            continue

        preds = bst.predict(df, iteration_range=(0, bst.best_iteration + 1))
        pred = preds > bkg_th

        df_pred = pd.DataFrame(df.get_data().toarray()[pred, :], columns=df.feature_names)
        if df_pred.size == 0:
            df_sig_passed_pred = excep(df_sig_passed_pred, df)
            continue
        df_pred['truth_label'] = key
        df_pred['predicts'] = df_pred[df.feature_names].to_numpy().argmax(axis=1)
        df_pred['weight'] = df.get_weight()[pred]
        df_sig_passed_pred = pd.concat([df_sig_passed_pred, df_pred])

        # if key != 2:
        passed_w += sum(df.get_weight()[pred])
        total_w += sum(df.get_weight())
        eff_dict.update({key: [sum(df.get_weight()[pred]), sum(df.get_weight())]})

    total_eff = passed_w / total_w if total_w != 0 else 0

    if debug: print(f'==> 0-bkg signal efficiency: {total_eff}')

    cm = np.zeros((3, 3))
    if total_eff > 0:
        cm = metrics.confusion_matrix(
            df_sig_passed_pred['truth_label'], df_sig_passed_pred['predicts'],
            # normalize='true',
            sample_weight=df_sig_passed_pred['weight'],
        )

    eff_dict.update({'cm': {
        key: cm[i, :].tolist()
        for i, key in enumerate(dsig.keys())
    }})

    # only for nDLLP-0 and nDLLP-1
    # df_nDLLP_0 = df_sig_passed_pred.loc[('truth_label' == 2) and ('predicts' == 3), 'nDLLP_1']
    # df_nDLLP_1 = df_sig_passed_pred.loc[('truth_label' == 3) and ('predicts' == 3), 'nDLLP_1']

    return total_eff, eff_dict, bst, bkg_th


def opt_train(args):
    # specify parameters
    train_param = {
        "tree_method": "gpu_hist" if platform.system() != 'Darwin' else 'hist',
        # "tree_method": "hist",
        "max_depth": int(args['max_depth']),
        "eta": float(args['eta']),
        "objective": "binary:logistic",
        'eval_metric': ['logloss'],
        'scale_pos_weight': args['weight_balance'],
    }

    total_eff, eff_dict, bst, bkg_cut = train_xgboost(train_param=train_param, **args['dataset'])

    return {'loss': -total_eff, 'status': STATUS_OK, 'eff': eff_dict, 'best_model': bst, 'bkg_cut': bkg_cut}


def opt(dataset, weight_balance, max_evals: int = 10):
    # define a search space
    space = {
        'max_depth': hp.quniform("max_depth", 2, 10, 1),
        'eta': hp.uniform('eta', 0.05, 0.5),
        'dataset': dataset,
        'weight_balance': weight_balance,
    }

    # minimize the objective over the space
    trials = Trials()

    best_hyperparams = fmin(fn=opt_train,
                            space=space,
                            algo=tpe.suggest,
                            max_evals=max_evals,
                            trials=trials)

    print("The best hyper parameters are : ", "\n")
    print(best_hyperparams)

    return trials
