import numpy as np
import pandas as pd
import pathlib
import os
import sys


def read_npz(sample: str, sample_total: float = 1.0):
    if not os.path.exists(sample):
        print(f'[Read samples] ==> {sample} does not exist. please check.')
        sys.exit(1)
    f = np.load(sample)
    df1 = pd.DataFrame(f['model_0'], columns=['2j_2fermion', '2j_4fermion', '2j_signal'])
    df2 = pd.DataFrame(f['model_1'], columns=['4j_2fermion', '4j_4fermion', '4j_signal'])
    df = pd.concat([df1, df2], axis=1)

    df['sample'] = pathlib.Path(sample).stem
    df['type'] = np.where(df['2j_signal'] >= df['4j_signal'], '2J', '4J')
    df['sample_weight'] = sample_total / len(df)

    return df
