import os

import numpy as np
import plotly.graph_objects as go


def normalize_cm(cm):
    new_matrix = [(np.array(l) / (sum(l) if sum(l) != 0 else 1) if -1 not in l else np.zeros(len(l))) for l in cm]

    return new_matrix


def plotting_cm(cm, cm_label, mass, lifetime, norm: bool = False, extra_title: str = None):
    if norm: cm = normalize_cm(cm)

    fig = go.Figure()
    fig.add_trace(
        go.Heatmap(
            z=cm, x=cm_label, y=cm_label, text=cm, texttemplate="%{text:.3f}",
            colorscale="Brwnyl")
    )
    fig.update_layout(autosize=False, height=600, width=1300)
    fig.update_yaxes(title={"text": 'truth label', "font": {'size': 18}})
    fig.update_xaxes(title={"text": 'predicted label', "font": {'size': 18}})
    fig.update_layout(
        width=800,
        height=700,
        # xaxis={'side': 'top'}
        title={
            'text': f"[{extra_title if extra_title is not None else ''}] Mass: {mass} [GeV], Lifetime: {lifetime} [ns]",
            'y': 0.9,
            'x': 0.5,
            'xanchor': 'center',
            'yanchor': 'top'},
    )

    if not os.path.exists("plots"):
        os.makedirs("plots")
    fig.write_image(
        os.path.join(
            'plots',
            f"out_{mass}_{lifetime}_{'scaled' if norm else 'raw'}"
            f"{'_' + extra_title if extra_title is not None else ''}.svg"
        )
    )


def plot_hist(df, bkg_cut):
    from sklearn import metrics
    from scipy import stats
    import matplotlib.pyplot as plt

    fig, ax = plt.subplots(1, 1, figsize=(8, 6), dpi=80)

    bins = np.linspace(0, 1, num=200, endpoint=True)
    bin_center = (bins[:-1] + bins[1:]) / 2.
    bin_err = bins[1:] - bin_center
    hist_style = dict(
        bins=bins,
        density=True, alpha=0.75, histtype='stepfilled', linewidth=1.5
    )

    hists = []

    sample_map = [
        {
            'cls': '#3d2247',
            'label': '2-fermion',
        },
        {
            'cls': '#5b148c',
            'label': '4-fermion',
        },
        {
            'cls': '#b86bc3',
            'label': '0-nDLLP',
        },
        {
            'cls': '#da91b7',
            'label': '1-nDLLP',
        },
        {
            'cls': '#dcdc85',
            'label': '2-nDLLP',
        },
    ]

    for sam in range(5):
        df_cut = df.loc[df['truth_label'] == sam]
        h, _, _ = ax.hist(
            df_cut['xgboost_score'],
            weights=df_cut['weight'],
            # color=sample_map[sam]['cls'],
            label=sample_map[sam]['label'],
            **hist_style
        )

    plt.xlabel('MVA score')
    ax.legend(loc=1)
    ax.set_yscale('log')
    plt.show()

    a = 0
